﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AppEntity.Models
{
    public class Customer
    {
        [Key] public int CustomerId { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Customer Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [MaxLength(250)]
        [DisplayName("Address")]
        [DataType(DataType.Text)]
        public string Address { get; set; }

        [Required]
        [MaxLength(150)]
        [DisplayName("Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required]
        [MaxLength(15)]
        [DisplayName("Phone Number")]
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }

        [Required]
        [DisplayName("Active Status")]
        public bool IsActive { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
        public virtual ICollection<Quotation> Quotations { get; set; }
    }
}