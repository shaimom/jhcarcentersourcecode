﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace AppEntity.Models
{
    public class Brand
    {
        [Key]
        public int BrandId { get; set; }
        
        [Required]
        [MaxLength(100)]
        [DisplayName("Brand Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        
        [Required]
        [DisplayName("Active Status")]
        public bool IsActive { get; set; }

        public virtual ICollection<Quotation> Quotations  { get; set; }
    }
}