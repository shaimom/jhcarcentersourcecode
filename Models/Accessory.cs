﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppEntity.Models
{
    public class Accessory
    {
        [Key] public int AccessoryId { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Accessory Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [Required]
        [DisplayName("Accessory Name")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }


        [Required]
        [DisplayName("Active Status")]
        public bool IsActive { get; set; }

    }
}