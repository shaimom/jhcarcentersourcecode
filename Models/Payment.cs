﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppEntity.Models
{
    public class Payment
    {
        [Key] public int PaymentId { get; set; }

        [Required]
        [DisplayName("Amount")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }

        [Required]
        [DisplayName("Amount")]
        [Range(1, int.MaxValue)]
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }
        
        [Required]
        [DisplayName("Payment Date")]
        [DataType(DataType.DateTime)]
        public DateTime PaymentDtTm { get; set; }

        public virtual Customer Customer { get; set; }
    }
}