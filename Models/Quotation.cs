﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AppEntity.Models
{
    public class Quotation
    {
        [Key] public int QuotationId { get; set; }

        [Required]
        [DisplayName("Quotation Date")]
        [DataType(DataType.DateTime)]
        public DateTime QuotationDate { get; set; }

        [Required]
        [DisplayName("Customer")]
        [Range(1, int.MaxValue)]
        [ForeignKey("Customer")]
        public int CustomerId { get; set; }

        [Required]
        [MaxLength(200)]
        [DisplayName("Vehicle Name")]
        [DataType(DataType.Text)]
        public string VehicleName { get; set; }


        [Required]
        [MaxLength(100)]
        [DisplayName("Model Name")]
        [DataType(DataType.Text)]
        public string ModelName { get; set; }


        [Required]
        [DisplayName("Price")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }


        [Required] 
        [DisplayName("Is Sold")] 
        public bool IsSold { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Chassis No")]
        [DataType(DataType.Text)]
        public string ChassisNo { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Engine No")]
        [DataType(DataType.Text)]
        public string EngineNo { get; set; }

        [Required]
        [DisplayName("Man Of Year")]
        [DataType(DataType.DateTime)]
        public DateTime ManOfYear { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Engine Size")]
        [DataType(DataType.Text)]
        public string EngineSize { get; set; }

        [Required]
        [DisplayName("Color Id")]
        [Range(1, int.MaxValue)]
        [ForeignKey("Color")]
        public int ColorId { get; set; }

        [Required]
        [DisplayName("Brand Id")]
        [Range(1, int.MaxValue)]
        [ForeignKey("Brand")]
        public int BrandId { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Seating Capacity")]
        [DataType(DataType.Text)]
        public string SeatingCapacity { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("FuelType")]
        [DataType(DataType.Text)]
        public string FuelType { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        [DisplayName("Fuel Capacity")]
        public int FuelCapacity { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Max Speed")]
        public string MaxSpeed { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Front Brake")]
        [DataType(DataType.Text)]
        public string FrontBrake { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Vehicle Name")]
        [DataType(DataType.Text)]
        public string RearBrake { get; set; }

        [Required]
        [MaxLength(100)]
        [DisplayName("Gear Type")]
        [DataType(DataType.Text)]
        public string GearType { get; set; }


        [Required]
        [DisplayName("OtherFeature")]
        [DataType(DataType.MultilineText)]
        public string OtherFeature { get; set; }

        [Required]
        [DisplayName("Required Doc")]
        [DataType(DataType.Text)]
        public string RequiredDoc { get; set; }

        [Required]
        [DisplayName("Accessory Items")]
        [DataType(DataType.Text)]
        public string AccessoryItems { get; set; }

        [Required]
        [DisplayName("Accessory Price")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal AccessoryPrice { get; set; }
        
        [Required]
        [DisplayName("Paid Amount")]
        [DataType(DataType.Currency)]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal PaidAmount { get; set; }

        [Required]
        [DisplayName("Created Date")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedDtTm { get; set; }

        [Required]
        [DisplayName("Updated Date")]
        [DataType(DataType.DateTime)]
        public DateTime UpdatedDtTm { get; set; }

        public virtual Customer Customer { get; set; }
        public virtual Color Color { get; set; }
        public virtual Brand Brand { get; set; }
    }
}