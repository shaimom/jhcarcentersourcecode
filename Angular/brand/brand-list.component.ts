import { Component, OnInit } from '@angular/core';
import {ApiResponse} from "../helpers/api.response";
import {Brand} from "./brand.model";
import {CustomerService} from "../customer/customer.service";
import {BrandService} from "./brand.service";

@Component({
  selector: 'app-brand-list',
  templateUrl: './brand-list.component.html',
  styles: [
  ]
})
export class BrandListComponent implements OnInit {
  brands: Brand[];
  constructor(private brandService: BrandService) {
  }
  ngOnInit(): void {
    this.getBrands();
  }

  getBrands(): void {
    this.brandService.getBrands()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.brands = res.Data;
          return;
        }
        alert(res.Message);
      }, error => {
        alert(error.message);
      });
  }

}
