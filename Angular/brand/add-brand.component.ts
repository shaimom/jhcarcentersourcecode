import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {BrandService} from "./brand.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ApiResponse} from "../helpers/api.response";

@Component({
  selector: 'app-add-brand',
  templateUrl: './add-brand.component.html'
})
export class AddBrandComponent implements OnInit {

  brandForm: FormGroup;
  brandId = this.activatedRoute.snapshot.params.id

  constructor(private formBuilder: FormBuilder,
              private brandService: BrandService,
              private  router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.brandForm = this.formBuilder.group({
      BrandId: [0],
      Name: new FormControl('', Validators.required),
      IsActive: new FormControl('', Validators.required),
    })
    if (this.brandId>0){
      this.getBrand();
    }
  }

  saveBrand() {
    this.brandService.addBrand(this.brandForm.value)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.router.navigate(['brand/brand-list'])
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      })
  }

  getBrand() {
    this.brandService.getBrand(this.brandId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.brandForm.patchValue(res.Data)
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      })
  }
}
