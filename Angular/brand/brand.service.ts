import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  constructor(private http: HttpClient) {
  }

  getBrands(): Observable<object> {
    return this.http.get(environment.apiUrl + "Brand")
  }

  addBrand(brand: any): Observable<object> {
    return this.http.post(environment.apiUrl + "Brand", brand);
  }

  getBrand(brandId: number): Observable<object> {
    return this.http.get(environment.apiUrl + "Brand/" + brandId);
  }
}
