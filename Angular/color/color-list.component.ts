import {Component, OnInit} from '@angular/core';
import {ApiResponse} from "../helpers/api.response";
import {ColorService} from "./color.service";
import {Color} from "./color.model";

@Component({
  selector: 'app-color-list',
  templateUrl: './color-list.component.html'
})
export class ColorListComponent implements OnInit {

  colors: Color[];

  constructor(private colorService: ColorService) {
  }

  ngOnInit(): void {
    this.getColors();
  }

  getColors(): void {
    this.colorService.getColors()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.colors = res.Data;
          return;
        }
        alert(res.Message);
      }, error => {
        alert(error.message);
      });
  }
}
