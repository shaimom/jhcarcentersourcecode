import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ColorService {

  constructor(private http: HttpClient) {
  }

  getColors(): Observable<object> {
    return this.http.get(environment.apiUrl + "Color")
  }

  addColor(color: any): Observable<object> {
    return this.http.post(environment.apiUrl + "Color", color);
  }

  getColor(colorId: number): Observable<object> {
    return this.http.get(environment.apiUrl + "Color/" + colorId);
  }
}
