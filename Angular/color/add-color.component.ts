import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ColorService} from "./color.service";
import {ApiResponse} from "../helpers/api.response";

@Component({
  selector: 'app-add-color',
  templateUrl: './add-color.component.html',
})
export class AddColorComponent implements OnInit {

  colorForm: FormGroup;
  colorId = this.activatedRoute.snapshot.params.id

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private activatedRoute: ActivatedRoute,
              private colorService: ColorService) {
  }

  ngOnInit(): void {
    this.colorForm = this.formBuilder.group({
      ColorId: [0],
      Name: new FormControl('', Validators.required),
      IsActive: new FormControl('', Validators.required)
    })
    if (this.colorId > 0) {
      this.getColor();
    }
  }

  saveColor() {
    this.colorService.addColor(this.colorForm.value)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.router.navigate(['color/color-list'])
        } else {
          alert((res.Message))
        }

      }, error => {
        alert(error.message)

      })
  }

  getColor() {
    this.colorService.getColor(this.colorId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.colorForm.patchValue(res.Data)
        } else {
          alert((res.Message))
        }

      }, error => {
        alert(error.message)

      })
  }
}
