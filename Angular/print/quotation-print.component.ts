import {Component, Input, OnInit} from '@angular/core';
import {ApiResponse} from "../helpers/api.response";
import {QuotationService} from "../quotation/quotation.service";
import {Quotation} from "../quotation/quotation.model";
import {Accessory} from "../accessory/accessory.model";

@Component({
  selector: 'app-quotation-print',
  templateUrl: './quotation-print.component.html',
  styleUrls: ['./quotation-print.component.css']
})
export class QuotationPrintComponent implements OnInit {

  quotation: Quotation;

  @Input()
  quotationId: number;

  constructor(private quotationService: QuotationService) {
  }

  ngOnInit(): void {
    this.getQuotation();
  }

  getQuotation(): void {
    this.quotationService.getQuotation(this.quotationId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.quotation = res.Data;
        }
      }, error => {
        alert(error.message);
      });
  }
}
