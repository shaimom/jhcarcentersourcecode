import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class AccessoryService {

  constructor(private http: HttpClient) {
  }

  getAccessories(): Observable<object> {
    return this.http.get(environment.apiUrl + "Accessory")
  }

  addAccessory(accessory: any): Observable<object> {
    return this.http.post(environment.apiUrl + "Accessory", accessory);
  }

  getAccessory(accessoryId: number): Observable<object> {
    return this.http.get(environment.apiUrl + "Accessory/" + accessoryId);
  }
}
