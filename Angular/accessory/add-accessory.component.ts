import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiResponse} from "../helpers/api.response";
import {AccessoryService} from "./accessory.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-accessory',
  templateUrl: './add-accessory.component.html'
})
export class AddAccessoryComponent implements OnInit {

  accessoryForm: FormGroup;
  accessoryId = this.activatedRoute.snapshot.params.id;

  constructor(private formBuilder: FormBuilder,
              private accessoryService: AccessoryService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.accessoryForm = this.formBuilder.group({
      AccessoryId: [0],
      Name: new FormControl('', Validators.required),
      Price: new FormControl('', Validators.required),
      IsActive: new FormControl('', Validators.required),
    });
    if (this.accessoryId > 0) {
      this.getAccessory()
    }
  }

  saveAccessory(): void {
    this.accessoryService.addAccessory(this.accessoryForm.value)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.router.navigate(["accessory/accessory-list"]);
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      });
  }

  getAccessory(): void {
    this.accessoryService.getAccessory(this.accessoryId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.accessoryForm.patchValue(res.Data);
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      });
  }
}
