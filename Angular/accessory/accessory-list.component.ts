import {Component, OnInit} from '@angular/core';
import {AccessoryService} from "./accessory.service";
import {ApiResponse} from "../helpers/api.response";
import {Accessory} from "./accessory.model";

@Component({
  selector: 'app-accessory-list',
  templateUrl: './accessory-list.component.html'
})
export class AccessoryListComponent implements OnInit {

  accessories: Accessory[];

  constructor(private accessoryService: AccessoryService) {
  }

  ngOnInit(): void {
    this.getAccessories();
  }

  getAccessories(): void {
    this.accessoryService.getAccessories()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.accessories = res.Data;
          return;
        }
        alert(res.Message);
      }, error => {
        alert(error.message);
      });
  }
}
