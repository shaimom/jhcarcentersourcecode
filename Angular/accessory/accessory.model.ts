export class Accessory {
  AccessoryId: number;
  Name: string;
  Price: number;
  IsActive: boolean;
}
