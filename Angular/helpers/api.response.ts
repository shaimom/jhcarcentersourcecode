export class ApiResponse {
  Success: boolean;
  Message: string;
  Data: any;
}
