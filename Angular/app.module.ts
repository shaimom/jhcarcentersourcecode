import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LayoutComponent} from './shared/layout.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CustomerListComponent} from "./customer/customer-list.component";
import {AddCustomerComponent} from './customer/add-customer.component';
import {AddColorComponent} from './color/add-color.component';
import {ColorListComponent} from './color/color-list.component';
import {BrandListComponent} from './brand/brand-list.component';
import {AddBrandComponent} from './brand/add-brand.component';
import {PaymentListComponent} from './payment/payment-list.component';
import {AddAccessoryComponent} from './accessory/add-accessory.component';
import {AccessoryListComponent} from './accessory/accessory-list.component';
import {QuotationListComponent} from './quotation/quotation-list.component';
import {AddQuotationComponent} from './quotation/add-quotation.component';
import {QuotationPrintComponent} from './print/quotation-print.component';
import {NgxSpinnerModule} from "ngx-spinner";
import { QuotationDetailsComponent } from './quotation/quotation-details.component';
import { AddPaymentComponent } from './payment/add-payment.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LayoutComponent,
    CustomerListComponent,
    AddCustomerComponent,
    AddColorComponent,
    ColorListComponent,
    BrandListComponent,
    AddBrandComponent,
    PaymentListComponent,
    AddAccessoryComponent,
    AccessoryListComponent,
    QuotationListComponent,
    AddQuotationComponent,
    QuotationPrintComponent,
    QuotationDetailsComponent,
    AddPaymentComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
