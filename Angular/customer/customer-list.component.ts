import { Component, OnInit } from '@angular/core';
import {ApiResponse} from "../helpers/api.response";
import {Customer} from "./customer.model";
import {CustomerService} from "./customer.service";

@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
})
export class CustomerListComponent implements OnInit {

  customers: Customer[];

  constructor(private customerService: CustomerService) {
  }

  ngOnInit(): void {
    this.getCustomers();
  }

  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.customers = res.Data;
          return;
        }
        alert(res.Message);
      }, error => {
        alert(error.message);
      });
  }

  printCustomer(){
    window.print();
  }

}
