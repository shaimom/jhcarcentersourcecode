import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {CustomerService} from "./customer.service";
import {ApiResponse} from "../helpers/api.response";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html'
})
export class AddCustomerComponent implements OnInit {

  customerId = this.activatedRoute.snapshot.params.id;

  customerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private customerService: CustomerService,
    private  router: Router,
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.customerForm = this.formBuilder.group({
      CustomerId: [0],
      Name: new FormControl('', Validators.required),
      Address: new FormControl('', Validators.required),
      Email: new FormControl('', Validators.required),
      Phone: new FormControl('', Validators.required),
      IsActive: new FormControl('', Validators.required)
    });

    if (this.customerId > 0) {
      this.getCustomer();
    }

  }

  saveCustomer(): void {
    this.customerService.addCustomer(this.customerForm.value)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.router.navigate(['customer/customer-list'])
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      });
  }

  getCustomer(): void {
    this.customerService.getCustomer(this.customerId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.customerForm.patchValue(res.Data);
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      });
  }
}
