export class Customer {
  CustomerId: number;
  Name: string;
  Address: string;
  Email: string;
  Phone: string;
  IsActive: boolean;
}
