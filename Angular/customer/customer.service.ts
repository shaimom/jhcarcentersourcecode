import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient) {
  }

  getCustomers(): Observable<object> {
    return this.http.get(environment.apiUrl + "Customer")
  }

  addCustomer(customer: any): Observable<object> {
    return this.http.post(environment.apiUrl + "Customer", customer);
  }

  getCustomer(customerId: number): Observable<object> {
    return this.http.get(environment.apiUrl + "Customer/" + customerId);
  }
}
