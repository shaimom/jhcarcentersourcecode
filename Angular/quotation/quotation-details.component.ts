import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {ApiResponse} from "../helpers/api.response";
import {Quotation} from "./quotation.model";
import {QuotationService} from "./quotation.service";

@Component({
  selector: 'app-quotation-details',
  templateUrl: './quotation-details.component.html'
})
export class QuotationDetailsComponent implements OnInit {

  quotation: Quotation;
  
  quotationId = this.activatedRoute.snapshot.params.id;

  constructor(private router: Router,
              private activatedRoute: ActivatedRoute,
              private quotationService : QuotationService) {
  }

  ngOnInit(): void {
    if (this.quotationId > 0) {
      this.getQuotation();
    }
  }

  getQuotation(): void {
    this.quotationService.getQuotation(this.quotationId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.quotation = res.Data;
        }
      }, error => {
        alert(error.message);
      });
  }

  printQuotation() {
    window.print()
  }
}
