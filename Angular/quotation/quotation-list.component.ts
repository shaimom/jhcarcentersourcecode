import {Component, OnInit} from '@angular/core';
import {ApiResponse} from "../helpers/api.response";
import {QuotationService} from "./quotation.service";
import {Quotation} from "./quotation.model";

@Component({
  selector: 'app-quotation-list',
  templateUrl: './quotation-list.component.html'
})
export class QuotationListComponent implements OnInit {

  quotationId = 0;
  quotations: Quotation[];

  constructor(private quotationService: QuotationService) {
  }

  ngOnInit(): void {
    this.getQuotations();
    console.log(this.quotationId)
  }

  getQuotations(): void {
    this.quotationService.getQuotations()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.quotations = res.Data;
          return;
        }
        alert(res.Message);
      }, error => {
        alert(error.message);
      });
  }

  printQuotation(quotationId: number): boolean {
    if (confirm('Do you want to print this ?')) {
      this.quotationId = quotationId;
      window.print();
    }
    return false;
  }
}
