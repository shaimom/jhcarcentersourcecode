import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {ApiResponse} from "../helpers/api.response";
import {CustomerService} from "../customer/customer.service";
import {Customer} from "../customer/customer.model";
import {AccessoryService} from "../accessory/accessory.service";
import {Accessory} from "../accessory/accessory.model";
import {BrandService} from "../brand/brand.service";
import {Brand} from "../brand/brand.model";
import {ColorService} from "../color/color.service";
import {Color} from "../color/color.model";
import {QuotationService} from "./quotation.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-add-quotation',
  templateUrl: './add-quotation.component.html'
})
export class AddQuotationComponent implements OnInit {

  quotationId = this.activatedRoute.snapshot.params.id;
  quotationForm: FormGroup;
  customers: Customer[];
  accessories: Accessory[];
  brands: Brand[];
  colors: Color[];
  accessoryItems = [];

  constructor(private customerService: CustomerService,
              private  accessoryService: AccessoryService,
              private formBuilder: FormBuilder,
              private brandService: BrandService,
              private colorService: ColorService,
              private quotationService: QuotationService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.getCustomers();
    this.getAccessories();
    this.getBrands();
    this.getColors();
    this.quotationForm = this.formBuilder.group({
      QuotationId: [0],
      QuotationDate: new FormControl('', Validators.required),
      CustomerId: new FormControl('', Validators.required),
      ChassisNo: new FormControl('', Validators.required),
      EngineNo: new FormControl('', Validators.required),
      ManOfYear: new FormControl('', Validators.required),
      EngineSize: new FormControl('', Validators.required),
      ColorId: new FormControl('', Validators.required),
      BrandId: new FormControl('', Validators.required),
      SeatingCapacity: new FormControl('', Validators.required),
      FuelType: new FormControl('', Validators.required),
      VehicleName: new FormControl('', Validators.required),
      FontBrake: new FormControl('', Validators.required),
      FuelCapacity: new FormControl('', Validators.required),
      MaxSpeed: new FormControl('', Validators.required),
      FrontBrake: new FormControl('', Validators.required),
      RearBrake: new FormControl('', Validators.required),
      GearType: new FormControl('', Validators.required),
      ModelName: new FormControl('', Validators.required),
      Price: new FormControl('', Validators.required),
      AccessoryItems: new FormControl('', Validators.required),
      AccessoryPrice: new FormControl('', Validators.required),
      OtherFeature: new FormControl('', Validators.required),
      RequiredDoc: new FormControl('', Validators.required)
    })
    if (this.quotationId > 0) {
      this.getQuotation();
    }
  }

  getCustomers(): void {
    this.customerService.getCustomers()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.customers = res.Data;
          return;
        }
      }, error => {
        alert(error.message);
      });
  }

  getAccessories(): void {
    this.accessoryService.getAccessories()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.accessories = res.Data;
        }
      }, error => {
        alert(error.message);
      });
  }

  getBrands(): void {
    this.brandService.getBrands()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.brands = res.Data;
        }
      }, error => {
        alert(error.message);
      });
  }

  getColors(): void {
    this.colorService.getColors()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.colors = res.Data;
        }
      }, error => {
        alert(error.message);
      });
  }

  getQuotation(): void {
    this.quotationService.getQuotation(this.quotationId)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.quotationForm.patchValue(res.Data)
        }
      }, error => {
        alert(error.message);
      });
  }

  saveQuotation() {
    console.log(this.quotationForm.value)
    this.quotationService.addQuotation(this.quotationForm.value)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.router.navigate(["quotation/quotation-list"])
        } else {
          alert(res.Message);
        }
      }, error => {
        alert(error.message);
      });
  }

  addAccessory(event: any, accessory: any) {
    const index = this.accessoryItems.indexOf(accessory.AccessoryId);
    if (index > -1) {
      this.accessoryItems.splice(index, 1);
    } else {
      this.accessoryItems.push(accessory.AccessoryId);
    }
    this.quotationForm.patchValue({
      AccessoryItems: this.accessoryItems.join(",")
    })
  }
}
