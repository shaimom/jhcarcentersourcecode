import {Accessory} from "../accessory/accessory.model";

export class Quotation {
  QuotationId: number;
  QuotationDate: Date;
  VehicleName: string;
  ModelName: string;
  Price: number;
  AccessoryPrice: number;
  AccessoryItems: string;
  Items: Accessory[];
  IsSold: boolean;
  CustomerName: string;
  CustomerId: number;
  Address: string;
  CreatedDtTm: Date;
  UpdatedDtTm: Date;
  ChassisNo: string;
  EngineNo: string;
  ManOfYear: Date;
  EngineSize: string;
  ColorId: number;
  ColorName: string;
  BrandId: number;
  BrandName: string;
  SeatingCapacity: string;
  FuelType: string;
  FuelCapacity: string;
  MaxSpeed: string;
  FrontBrake: string;
  RearBrake: string;
  GearType: string;
  PaidAmount: number;
  OtherFeature: Text;
  RequiredDoc: Text;
}
