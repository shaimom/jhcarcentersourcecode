import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class QuotationService {

  constructor(private http: HttpClient) {
  }

  getQuotations(): Observable<object> {
    return this.http.get(environment.apiUrl + "Quotation")
  }

  addQuotation(quotation: any): Observable<object> {
    return this.http.post(environment.apiUrl + "Quotation", quotation);
  }

  getQuotation(quotationId: number): Observable<object> {
    return this.http.get(environment.apiUrl + "Quotation/" + quotationId);
  }
}
