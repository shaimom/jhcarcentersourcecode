import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {PaymentService} from "./payment.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ApiResponse} from "../helpers/api.response";

@Component({
  selector: 'app-add-payment',
  templateUrl: './add-payment.component.html',

})
export class AddPaymentComponent implements OnInit {

  quotationId = this.activatedRoute.snapshot.params.id;
  paymentForm: FormGroup;

  constructor(private  formBuilder: FormBuilder,
              private paymentService: PaymentService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.paymentForm = this.formBuilder.group({
      QuotationId: this.quotationId,
      PaidAmount: new FormControl('', Validators.required),
      IsSold: true
    })
  }

  savePayment() {
    this.paymentService.addPayment(this.paymentForm.value)
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.router.navigate(["payment/payment-list"])
        } else {
          alert(res.Message)
        }
      }, error => {
      })
  }
  printQuotation(){
    window.print();
  }
}
