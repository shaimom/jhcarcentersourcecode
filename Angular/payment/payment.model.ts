export class Payment {
  QuotationId: number;
  PaidAmount: number;
  IsSold: boolean;
}
