import { Component, OnInit } from '@angular/core';
import {ApiResponse} from "../helpers/api.response";
import {QuotationService} from "../quotation/quotation.service";
import {Quotation} from "../quotation/quotation.model";

@Component({
  selector: 'app-payment-list',
  templateUrl: './payment-list.component.html',

})
export class PaymentListComponent implements OnInit {
  quotations: Quotation[];

  constructor(private quotationService:QuotationService) { }

  ngOnInit(): void {
    this.getPayments()
  }
  getPayments(): void {
    this.quotationService.getQuotations()
      .subscribe((res: ApiResponse) => {
        if (res.Success) {
          this.quotations = res.Data;
          return;
        }
        alert(res.Message);
      }, error => {
        alert(error.message);
      });
  }
}
