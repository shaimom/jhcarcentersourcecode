import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  constructor(private http: HttpClient) {
  }

  getPayments(): Observable<object> {
    return this.http.get(environment.apiUrl + "Payment")
  }

  addPayment(payment:any): Observable<object> {
    return this.http.post(environment.apiUrl + "Quotation/AddPayment", payment)
  }

}
