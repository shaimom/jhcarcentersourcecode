import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {LayoutComponent} from "./shared/layout.component";
import {AccessoryListComponent} from "./accessory/accessory-list.component";
import {AddAccessoryComponent} from "./accessory/add-accessory.component";
import {BrandListComponent} from "./brand/brand-list.component";
import {AddBrandComponent} from "./brand/add-brand.component";
import {ColorListComponent} from "./color/color-list.component";
import {AddColorComponent} from "./color/add-color.component";
import {CustomerListComponent} from "./customer/customer-list.component";
import {AddCustomerComponent} from "./customer/add-customer.component";
import {PaymentListComponent} from "./payment/payment-list.component";
import {QuotationListComponent} from "./quotation/quotation-list.component";
import {AddQuotationComponent} from "./quotation/add-quotation.component";
import {QuotationDetailsComponent} from "./quotation/quotation-details.component";
import {AddPaymentComponent} from "./payment/add-payment.component";


const routes: Routes = [{
  path: '',
  component: LayoutComponent,
  children: [{
    path: '',
    component: DashboardComponent,
  }, {
    path: 'accessory/accessory-list',
    component: AccessoryListComponent
  }, {
    path: 'accessory/add-accessory',
    component: AddAccessoryComponent
  }, {
    path: 'accessory/add-accessory/:id',
    component: AddAccessoryComponent
  }, {
    path: 'brand/brand-list',
    component: BrandListComponent
  }, {
    path: 'brand/add-brand',
    component: AddBrandComponent
  }, {
    path: 'brand/add-brand/:id',
    component: AddBrandComponent
  }, {
    path: 'color/color-list',
    component: ColorListComponent
  }, {
    path: 'color/add-color',
    component: AddColorComponent
  }, {
    path: 'color/add-color/:id',
    component: AddColorComponent
  }, {
    path: 'customer/customer-list',
    component: CustomerListComponent
  }, {
    path: 'customer/add-customer',
    component: AddCustomerComponent
  }, {
    path: 'customer/add-customer/:id',
    component: AddCustomerComponent
  }, {
    path: 'payment/add-payment/:id',
    component: AddPaymentComponent
  }, {
    path: 'payment/payment-list',
    component: PaymentListComponent
  }, {
    path: 'quotation/quotation-list',
    component: QuotationListComponent
  }, {
    path: 'quotation/add-quotation',
    component: AddQuotationComponent
  }, {
    path: 'quotation/add-quotation/:id',
    component: AddQuotationComponent
  }, {
    path: 'quotation/quotation-details/:id',
    component: QuotationDetailsComponent
  }]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
