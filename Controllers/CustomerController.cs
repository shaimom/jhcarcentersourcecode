﻿namespace AdminApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomer _customer;

        public CustomerController(ICustomer customer)
        {
            _customer = customer;
        }

        [HttpGet]
        public async Task<ActionResult<Hashtable>> GetCustomers()
        {
            var result = await _customer.GetCustomers();
            if (!result.Any())
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }

        [HttpPost]
        public async Task<ActionResult<Hashtable>> PostCustomer(Customer customer)
        {
            var result = await _customer.AddCustomer(customer);
            if (result <= 0)
            {
                return Messages.SaveFailed();
            }

            return Messages.SaveSuccess();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Hashtable>> GetCustomer(int id)
        {
            var result = await _customer.GetCustomer(id);

            if (result == null)
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }
    }
}