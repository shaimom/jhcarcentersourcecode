﻿namespace AdminApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuotationController : ControllerBase
    {
        private readonly IQuotation _quotation;

        public QuotationController(IQuotation quotation)
        {
            _quotation = quotation;
        }

        [HttpGet]
        public async Task<Hashtable> GetQuotations()
        {
            var result = await _quotation.GetQuotations();
            if (!result.Any())
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }

        [HttpPost]
        public async Task<Hashtable> PostQuotations(Quotation quotation)
        {
            var result = await _quotation.AddQuotation(quotation);
            if (result <= 0)
            {
                return Messages.SaveFailed();
            }

            return Messages.SaveSuccess();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Hashtable>> GetQuotation(int id)
        {
            var result = await _quotation.GetQuotation(id);

            if (result == null)
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }

        [HttpPost]
        [Route("AddPayment")]
        public async Task<Hashtable> AddPayment(QPayment quotation)
        {
            var result = await _quotation.AddPayment(quotation);
            if (result <= 0)
            {
                return Messages.SaveFailed();
            }

            return Messages.SaveSuccess();
        }
    }
}