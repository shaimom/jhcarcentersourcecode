﻿namespace AdminApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ColorController : ControllerBase
    {
        private readonly IColor _color;

        public ColorController(IColor color )
        {
            _color = color;
        }

        [HttpGet]
        public async Task<ActionResult<Hashtable>> GetCustomers()
        {
            var result = await _color.GetColors();
            if (!result.Any())
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }

        [HttpPost]
        public async Task<ActionResult<Hashtable>> PostColor(Color color)
        {
            var result = await _color.AddColor(color);
            if (result <= 0)
            {
                return Messages.SaveFailed();
            }

            return Messages.SaveSuccess();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Hashtable>> GetColor(int id)
        {
            var result = await _color.GetColor(id);

            if (result == null)
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }
    }
}
