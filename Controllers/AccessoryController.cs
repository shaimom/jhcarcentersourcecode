﻿namespace AdminApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccessoryController : ControllerBase
    {
        private readonly IAccessory _accessory;

        public AccessoryController(IAccessory accessory)
        {
            _accessory = accessory;
        }

        [HttpGet]
        public async Task<ActionResult<Hashtable>> GetAccessories()
        {
            var result = await _accessory.GetAccessories();

            if (!result.Any())
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Hashtable>> GetAccessory(int id)
        {
            var result = await _accessory.GetAccessory(id);

            if (result == null)
            {
                return Messages.EmptyData();
            }

            return Messages.GetData(result);
        }

        [HttpPost]
        public async Task<ActionResult<Hashtable>> PostAccessory(Accessory accessory)
        {
            var result = await _accessory.AddAccessory(accessory);
            if (result <= 0)
            {
                return Messages.SaveFailed();
            }

            return Messages.SaveSuccess();
        }
    }
}