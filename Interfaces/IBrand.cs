﻿using AppEntity.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppContract.Interfaces
{
   public interface IBrand
    {
        Task<IEnumerable<Brand>> GetBrands();
        Task<int> AddBrand(Brand brand);
        Task<Brand> GetBrand(int id);
    }

}
