﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppContract.VModels;
using AppEntity.Models;

namespace AppContract.Interfaces
{
    public interface IQuotation
    {
        Task<IEnumerable<object>> GetQuotations();
        Task<int> AddQuotation(Quotation quotation);
        Task<int> AddPayment(QPayment quotation);
        Task<object> GetQuotation(int id);
    }
}