﻿using AppEntity.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppContract.Interfaces
{
    public interface IAccessory
    {
        Task<IEnumerable<Accessory>> GetAccessories();
        Task<int> AddAccessory(Accessory accessory);
        Task<Accessory> GetAccessory(int id);
    }
}
