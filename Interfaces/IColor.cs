﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppEntity.Models;

namespace AppContract.Interfaces
{
    public interface IColor
    {
        Task<IEnumerable<Color>> GetColors();
        Task<int> AddColor(Color color);
        Task<Color> GetColor(int id);
    }
}