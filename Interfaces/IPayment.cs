﻿using AppEntity.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AppContract.Interfaces
{
   public interface IPayment
    {
        Task<IEnumerable<object>> GetPayments();

    }
}
