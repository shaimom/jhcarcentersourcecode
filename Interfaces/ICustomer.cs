﻿using AppEntity.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AppContract.Interfaces
{
   public interface ICustomer
    {
        Task<IEnumerable<Customer>> GetCustomers();
        Task<int> AddCustomer(Customer customer);
        Task<Customer> GetCustomer(int id);
    }
}
