﻿namespace AppContract.Repositories
{
    public class RAccessory : IAccessory
    {
        private readonly DbEntities _entities;

        public RAccessory(DbEntities entities)
        {
            _entities = entities;
        }

        public async Task<IEnumerable<Accessory>> GetAccessories()
        {
            return await _entities.Accessories.ToListAsync();
        }

        public async Task<Accessory> GetAccessory(int id)
        {
            return await _entities.Accessories.FindAsync(id);
        }

        public async Task<int> AddAccessory(Accessory accessory)
        {
            if (accessory.AccessoryId > 0)
            {
                _entities.Accessories.Update(accessory);
            }
            else
            {
                await _entities.Accessories.AddAsync(accessory);
            }

            return await _entities.SaveChangesAsync();
        }
    }
}