﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppContract.Interfaces;
using AppEntity.Database;
using AppEntity.Models;
using Microsoft.EntityFrameworkCore;

namespace AppContract.Repositories
{
    public class RColor : IColor
    {
        private readonly DbEntities _entities;

        public RColor(DbEntities entities)
        {
            _entities = entities;
        }

        public async Task<IEnumerable<Color>> GetColors()
        {
            return await _entities.Colors.ToListAsync();
        }

        public async Task<Color> GetColor(int id)
        {
            return await _entities.Colors.FindAsync(id);
        }

        public async Task<int> AddColor(Color color)
        {
            if (color.ColorId > 0)
            {
                _entities.Colors.Update(color);
            }
            else
            {
                await _entities.Colors.AddAsync(color);
            }

            return await _entities.SaveChangesAsync();
        }
    }
}