﻿namespace AppContract.Repositories
{
    public class RQuotation : IQuotation
    {
        private readonly DbEntities _entities;

        public RQuotation(DbEntities entities)
        {
            _entities = entities;
        }

        public async Task<IEnumerable<object>> GetQuotations()
        {
            return await _entities.Quotations
                .Include(x => x.Customer)
                .Include(x => x.Brand)
                .Include(x => x.Color)
                .Select(x => new
                {
                    x.QuotationId,
                    x.QuotationDate,
                    x.VehicleName,
                    x.ModelName,
                    x.Price,
                    x.CustomerId,
                    x.IsSold,
                    x.ColorId,
                    x.BrandId,
                    x.ChassisNo,
                    x.EngineNo,
                    x.ManOfYear,
                    x.EngineSize,
                    x.SeatingCapacity,
                    x.FuelType,
                    x.FuelCapacity,
                    x.MaxSpeed,
                    x.FrontBrake,
                    x.RearBrake,
                    x.GearType,
                    x.OtherFeature,
                    x.RequiredDoc,
                    x.CreatedDtTm,
                    x.UpdatedDtTm,
                    x.AccessoryItems,
                    x.AccessoryPrice,
                    x.PaidAmount,
                    CustomerName = x.Customer.Name,
                    ColorName = x.Color.Name,
                    BrandName = x.Brand.Name
                })
                .ToListAsync();
        }

        public async Task<object> GetQuotation(int id)
        {
            var acc = await _entities.Quotations.FindAsync(id);
            var accItems = acc.AccessoryItems.Split(",").Select(int.Parse).ToList();
            var selectAcc = _entities.Accessories
                .Where(p =>
                    accItems.Any(y =>
                        y == p.AccessoryId)
                )
                .Select(x => new
                {
                    x.AccessoryId,
                    x.Name,
                    x.Price
                })
                .ToList();

            return await _entities.Quotations
                .Where(x => x.QuotationId == id)
                .Include(x => x.Customer)
                .Include(x => x.Brand)
                .Include(x => x.Color)
                .Select(x => new
                {
                    x.QuotationId,
                    x.QuotationDate,
                    x.VehicleName,
                    x.ModelName,
                    x.CustomerId,
                    x.Price,
                    x.IsSold,
                    x.ChassisNo,
                    x.EngineNo,
                    x.ManOfYear,
                    x.EngineSize,
                    x.SeatingCapacity,
                    x.FuelType,
                    x.FuelCapacity,
                    x.MaxSpeed,
                    x.ColorId,
                    x.BrandId,
                    x.FrontBrake,
                    x.RearBrake,
                    x.GearType,
                    x.OtherFeature,
                    x.RequiredDoc,
                    x.CreatedDtTm,
                    x.UpdatedDtTm,
                    x.AccessoryItems,
                    x.AccessoryPrice,
                    x.PaidAmount,
                    CustomerName = x.Customer.Name,
                    ColorName = x.Color.Name,
                    BrandName = x.Brand.Name,
                    Items = selectAcc
                })
                .FirstOrDefaultAsync();
        }

        public async Task<int> AddQuotation(Quotation quotation)
        {
            if (quotation.QuotationId > 0)
            {
                _entities.Quotations.Update(quotation);
            }
            else
            {
                await _entities.Quotations.AddAsync(quotation);
            }

            return await _entities.SaveChangesAsync();
        }


        public async Task<int> AddPayment(QPayment quotation)
        {
            var req = _entities.Quotations.Find(quotation.QuotationId);
            req.PaidAmount = quotation.PaidAmount;
            req.IsSold = quotation.IsSold;

            _entities.Quotations.Update(req);

            return await _entities.SaveChangesAsync();
        }
    }
}