﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AppContract.Interfaces;
using AppEntity.Database;
using AppEntity.Models;
using Microsoft.EntityFrameworkCore;

namespace AppContract.Repositories
{
    public class RBrand : IBrand
    {
        private readonly DbEntities _entities;

        public RBrand(DbEntities entities)
        {
            _entities = entities;
        }

        public async Task<IEnumerable<Brand>> GetBrands()
        {
            return await _entities.Brands.ToListAsync();
        }

        public async Task<Brand> GetBrand(int id)
        {
            return await _entities.Brands.FindAsync(id);
        }

        public async Task<int> AddBrand(Brand brand)
        {
            if (brand.BrandId > 0)
            {
                _entities.Brands.Update(brand);
            }
            else
            {
                await _entities.Brands.AddAsync(brand);
            }

            return await _entities.SaveChangesAsync();
        }
    }
}