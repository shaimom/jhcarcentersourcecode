﻿using AppContract.Interfaces;
using AppEntity.Database;
using AppEntity.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppContract.Repositories
{
    public class RPayment : IPayment
    {
        private readonly DbEntities _entities;

        public RPayment(DbEntities entities)
        {
            _entities = entities;
        }
        public async Task<IEnumerable<object>> GetPayments()
        {
            return await _entities.Payments
                .Include(x=>x.Customer)
                .Select(x=> new { 
                x.Amount,
                x.Customer.Name,
                x.PaymentDtTm
                })
                .ToListAsync();
        }
    }
}
