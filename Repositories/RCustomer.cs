﻿namespace AppContract.Repositories
{
    public class RCustomer : ICustomer
    {
        private readonly DbEntities _entities;

        public RCustomer(DbEntities entities)
        {
            _entities = entities;
        }

        public async Task<IEnumerable<Customer>> GetCustomers()
        {
            return await _entities.Customers.ToListAsync();
        }

        public async Task<Customer> GetCustomer(int id)
        {
            return await _entities.Customers.FindAsync(id);
        }

        public async Task<int> AddCustomer(Customer customer)
        {
            if (customer.CustomerId > 0)
            {
                _entities.Customers.Update(customer);
            }
            else
            {
                await _entities.Customers.AddAsync(customer);
            }

            return await _entities.SaveChangesAsync();
        }
    }
}